import React from "react";
import ItemList from "../item-list";
import PersonDetails from "../person-details";

class PeoplePage extends React.Component{
    state = {
        selectedItem: 4
    }
    onSelectItem = (id) => {
     this.setState(
         {
             selectedItem: id
         }
     )
    }
    render() {
        console.log('selected item >>>>>>>>>>>>>', this.state.selectedItem)
        return(
            <div className="row mb2">
                <div className="col-md-6">
                    <ItemList  onSelectItem={this.onSelectItem} />
                </div>
                <div className="col-md-6">
                    <PersonDetails selectedItem={this.state.selectedItem} />
                </div>
            </div>
        )
    }
}

export default PeoplePage;