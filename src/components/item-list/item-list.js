import React, { Component } from 'react';
import SwapiService from "../../services/swapi-service";
import Loader from "../loading";

import './item-list.css';

export default class ItemList extends Component {
    state = {
       data: [],
       loading:true,
    }

    swapi = new SwapiService()
    componentDidMount = () => {

        this.swapi.getAllPeople().then((data) => {
            this.setState({
                data: data,
                loading: false
            })
        })
    }


    render() {
        if (this.state.loading) {
            return <Loader />
        } else {
            const content = this.state.data.map((item) => {
                return (
                    <li key={item.id} onClick={() => this.props.onSelectItem(item.id)} className="list-group-item">
                        {`${item.name} -(${item.gender})`}
                    </li>
                )
            })
            return (
                <ul className="item-list list-group">
                    {content}
                </ul>
            );
        }
    }
}
