import React, { Component } from 'react';
import SwapiService from "../../services/swapi-service";
import Loader from "../loading";
import './person-details.css';

export default class PersonDetails extends Component {
  state = {
    data: {},
      loading: true,
  }
  swapi = new SwapiService()

  componentDidMount = () => {
    const id = this.props.selectedItem
   this.swapi.getPerson(id).then(data => {
      this.setState({
        data: data,
        loading: false
      })
    })
  }

   componentDidUpdate = (prevProps, prevState) => {
         console.log('>>> Update >>>', this.props, prevProps, prevState)

       if (this.props.selectedItem != prevProps.selectedItem)  {
             const id = this.props.selectedItem
             this.swapi.getPerson(id).then(data => {
                 this.setState({
                     data: data
        })
        })
       }
   }

     render() {
    const {id, name, gender, birthYear, eyeColor} = this.state.data;
    const imageUrl = `https://starwars-visualguide.com/assets/img/characters/${id}.jpg`

         if (this.state.loading) {
             return <Loader />
         } else {
             return (
                 <div className="person-details card">
                     <img className="person-image"
                          src={imageUrl} />

                     <div className="card-body">
                         <h4>{name}</h4>
                         <ul className="list-group list-group-flush">

                             <li className="list-group-item">
                                 <span className="term">Gender</span>
                                 <span>{gender}</span>
                             </li>

                             <li className="list-group-item">
                                 <span className="term">Birth Year</span>
                                 <span>{birthYear}</span>
                             </li>

                             <li className="list-group-item">
                                 <span className="term">Eye Color</span>
                                 <span>{eyeColor}</span>
                             </li>
                         </ul>
                     </div>
                 </div>
             )
         }

  }
}
